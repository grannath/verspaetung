# Mobimeo Challenge "Verspaetung"

This is my solution to the given challenge.
It is a Spring Boot application written in Kotlin.

## Technology Choice

I tried setting up the solution in Scala, but had issues with my development environment.
So I stuck to what I know for this time.

When it comes to web applications, Spring Boot is simply the most productive.
It includes integrations for basically anything.
This makes it very easy to set up any type of application.

## API

A basic API documentation can be inspected as the home page of the application.
By default this is http://localhost:8081.

## Challenge Tasks

1.  The nest vehicle arriving at a stop can be retrieved with `/stops/{id]/nextVehicle`.
    Optionally, a query parameter `time` can be passed.
    By default the request uses the current time.
2.  The response for the next vehicle at a stop also includes the delay of that line at that stop.
    It's technically trivial to add the delay also to the line lookup, since it is constant here.
    I decided not to as the constant delay is only an assumption to simplify the challenge.
    I prefer to have the API more realistic.
3.  Under `/lines/{id}/position` the current position of a vehicle can be retrieved.
    It also accepts a `time` parameter to get the position at a different point in time.
    In a more realistic scenario, this would have to give either multiple vehicles, or add a subresource.

## Further Considerations

The app includes metrics via Spring Actuator.
No further configuration has been added, since no requirements are given.
But exposing Prometheus metrics, or any other format, health checks, logs, etc. is merely a matter of configuration now.

No security is included here.
First, it makes testing the app more annoying and is easy to add with Spring Security.
Also, in my opinion security should be moved into the infrastructure, using an Ingress Controller, Service Mesh, or similar.

Storage of data is simplified to maps.
Depending on the real situation, a normal RDS system, an in-memory database, a graph database or something else might be best.
Without more data, anything else than a map is just overkill.
