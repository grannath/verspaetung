package de.grannath.verspaetung

import io.kotlintest.shouldBe
import io.kotlintest.shouldThrowExactly
import io.kotlintest.specs.StringSpec
import io.kotlintest.tables.forAll
import io.kotlintest.tables.headers
import io.kotlintest.tables.row
import io.kotlintest.tables.table
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.time.LocalTime

@ExperimentalCoroutinesApi
class FindVehiclePositionTest : StringSpec({
    "handles vehicles before tour" {
        shouldThrowExactly<UnknownPositionException> { findVehiclePosition(LineId(1), LocalTime.of(9, 0)) }
    }

    "handles vehicles after tour" {
        shouldThrowExactly<UnknownPositionException> { findVehiclePosition(LineId(1), LocalTime.of(11, 0)) }
    }

    "finds position of line 0 during tour" {
        table(
            headers("time", "coordinate"),
            row(LocalTime.of(10, 1), Coordinate(1, 1)),
            row(LocalTime.of(10, 2), Coordinate(1, 2)),
            row(LocalTime.of(10, 4), Coordinate(1, 4)),
            row(LocalTime.of(10, 7), Coordinate(2, 7))
        ).forAll { time, coordinate ->
            findVehiclePosition(LineId(0), time) shouldBe coordinate
        }
    }
})
