package de.grannath.verspaetung

import io.kotlintest.shouldBe
import io.kotlintest.shouldThrowExactly
import io.kotlintest.specs.StringSpec
import io.kotlintest.tables.forAll
import io.kotlintest.tables.headers
import io.kotlintest.tables.row
import io.kotlintest.tables.table
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.time.LocalTime

@ExperimentalCoroutinesApi
class NextVehicleTest : StringSpec({
    "Handles unknown stops" {
        shouldThrowExactly<UnknownStopException> { findNextVehicle(StopId(11111)) }
    }

    "Finds a next vehicle" {
        table(
            headers("stopId", "time", "lineId", "arrival"),
            row(StopId(6), LocalTime.of(10, 0), LineId(1), LocalTime.of(10, 6)),
            row(StopId(3), LocalTime.of(10, 9), LineId(1), LocalTime.of(10, 10)),
            row(StopId(3), LocalTime.of(10, 20), LineId(0), LocalTime.of(10, 8))
        ).forAll { stopId, time, lineId, arrival ->
            findNextVehicle(stopId, time).apply {
                line.lineId shouldBe lineId
                estimatedArrival shouldBe arrival
            }
        }
    }
})
