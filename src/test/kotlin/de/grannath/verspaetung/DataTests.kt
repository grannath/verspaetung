package de.grannath.verspaetung

import io.kotlintest.matchers.haveSize
import io.kotlintest.matchers.maps.shouldContain
import io.kotlintest.should
import io.kotlintest.specs.StringSpec
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.time.LocalTime

@ExperimentalCoroutinesApi
class ParsingTest : StringSpec({
    "delays contains a data set" {
        delays.keys should haveSize(3)

        delays shouldContain (LineName("M4") to Delay(LineName("M4"), 1))
    }

    "lines contains a data set" {
        lines.keys should haveSize(3)

        lines shouldContain (LineId(1) to Line(LineId(1), LineName("200")))
    }

    "stops contains a data set" {
        stops.keys should haveSize(12)

        stops shouldContain (StopId(4) to Stop(StopId(4), Coordinate(3, 11)))
    }

    "times contains a data set" {
        times.keys should haveSize(15)

        times shouldContain (LineId(1) to StopId(7) to Time(LineId(1), StopId(7), LocalTime.of(10, 6)))
    }
})
