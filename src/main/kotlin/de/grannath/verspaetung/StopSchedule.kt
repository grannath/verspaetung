package de.grannath.verspaetung

import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.time.Duration
import java.time.LocalTime

data class NextVehicle(
    val line: Line,
    val plannedArrival: LocalTime,
    val estimatedArrival: LocalTime,
    val delay: Duration
)

@ExperimentalCoroutinesApi
fun findNextVehicle(stop: StopId, time: LocalTime = LocalTime.now()): NextVehicle =
    times.values.filter { it.stopId == stop }
        .apply { ifEmpty { throw UnknownStopException(stop) } }
        .run {
            filter { it.estimatedArrival() >= time }
                .minBy { it.estimatedArrival() }
                // If there is no next one, roll over to next day
                ?: minBy { it.estimatedArrival() }!!
        }
        .let {
            NextVehicle(
                line = lines[it.lineId]!!,
                plannedArrival = it.time,
                estimatedArrival = it.estimatedArrival(),
                delay = Duration.ofMinutes(it.delay()?.delay?.toLong() ?: 0)
            )
        }
