package de.grannath.verspaetung

import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.time.LocalTime
import java.time.temporal.ChronoUnit
import java.time.temporal.Temporal
import kotlin.math.abs
import kotlin.math.sign

/**
 * Calculates the current position of a line.
 *
 * Assumes that vehicles always travel along the x axis first. Also always rounds coordinates. In a real application,
 * rounding and precision need to be handled better. But given the system here, it's a little futile to bother too much.
 */
@ExperimentalCoroutinesApi
fun findVehiclePosition(line: LineId, time: LocalTime = LocalTime.now()): Coordinate {
    val prevStop = previousOrCurrentStop(line, time)
    val nextStop = nextStop(line, time)
    return percentPositionBetween(
        stops[prevStop.stopId]?.coordinate ?: throw UnknownStopException(prevStop.stopId),
        stops[nextStop.stopId]?.coordinate ?: throw UnknownStopException(nextStop.stopId),
        percentDoneBetween(
            prevStop,
            nextStop,
            time
        )
    )
}

private fun percentPositionBetween(start: Coordinate, stop: Coordinate, percentDone: Long): Coordinate {
    val totalDistance = abs(start distanceX stop) + abs(start distanceY stop)
    val distanceDone = totalDistance * percentDone / 100
    return if (distanceDone <= abs(start distanceX stop)) {
        start.plusX(distanceDone * (start distanceX stop).sign)
    } else {
        stop.plusY((distanceDone - totalDistance) * (start distanceY stop).sign)
    }
}

private infix fun Coordinate.distanceX(other: Coordinate) =
    other.x - this.x

private infix fun Coordinate.distanceY(other: Coordinate) =
    other.y - this.y

private infix fun Coordinate.plusX(dx: Long) =
    copy(x = this.x + dx)

private infix fun Coordinate.plusY(dy: Long) =
    copy(y = this.y + dy)

@ExperimentalCoroutinesApi
private fun percentDoneBetween(start: Time, stop: Time, now: LocalTime): Long =
    100 * (start minutesUntil now) / (start minutesUntil stop.estimatedArrival())

@ExperimentalCoroutinesApi
private infix fun Time.minutesUntil(other: Temporal) =
    this.estimatedArrival().until(other, ChronoUnit.MINUTES)

@ExperimentalCoroutinesApi
private fun previousOrCurrentStop(lineId: LineId, time: LocalTime): Time =
    times.values.filter { it.lineId == lineId }
        .apply { ifEmpty { throw UnknownLineException(lineId) } }
        .filter { it.estimatedArrival() <= time }
        .maxBy { it.estimatedArrival() }
        ?: throw UnknownPositionException("The line has not started it's tour yet.")

@ExperimentalCoroutinesApi
private fun nextStop(lineId: LineId, time: LocalTime): Time =
    times.values.filter { it.lineId == lineId }
        .apply { ifEmpty { throw UnknownLineException(lineId) } }
        .filter { it.estimatedArrival() > time }
        .minBy { it.estimatedArrival() }
        ?: throw UnknownPositionException("The line has already finished it's tour.")

class UnknownPositionException(msg: String) : RuntimeException(msg)
