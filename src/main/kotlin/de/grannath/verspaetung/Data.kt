package de.grannath.verspaetung

import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.time.LocalTime
import java.time.format.DateTimeFormatter

@ExperimentalCoroutinesApi
private fun readCsvFile(inputStream: InputStream) =
    BufferedReader(InputStreamReader(inputStream))
        .lineSequence()
        .drop(1)
        .map { it.split(",") }

@ExperimentalCoroutinesApi
private fun <D, K> readAndSortData(path: String, key: (D) -> K, mapper: (List<String>) -> D) =
    readCsvFile(VerspaetungApplication::class.java.getResourceAsStream(path))
        .map(mapper)
        .associateBy(key)

inline class LineName(val name: String)
inline class LineId(val id: Int)
inline class StopId(val id: Int)

data class Delay(val lineName: LineName, val delay: Int)

@ExperimentalCoroutinesApi
val delays: Map<LineName, Delay> by lazy {
    readAndSortData("/data/delays.csv", Delay::lineName) { Delay(LineName(it[0]), it[1].toInt()) }
}

data class Line(val lineId: LineId, val lineName: LineName)

@ExperimentalCoroutinesApi
val lines: Map<LineId, Line> by lazy {
    readAndSortData("/data/lines.csv", Line::lineId) { Line(LineId(it[0].toInt()), LineName(it[1])) }
}

data class Coordinate(val x: Long, val y: Long)

data class Stop(val stopId: StopId, val coordinate: Coordinate)

@ExperimentalCoroutinesApi
val stops: Map<StopId, Stop> by lazy {
    readAndSortData("/data/stops.csv", Stop::stopId) {
        Stop(
            StopId(it[0].toInt()),
            Coordinate(it[1].toLong(), it[2].toLong())
        )
    }
}

data class Time(val lineId: LineId, val stopId: StopId, val time: LocalTime)

val Time.key: Pair<LineId, StopId>
    get() = lineId to stopId

@ExperimentalCoroutinesApi
fun Time.estimatedArrival(): LocalTime =
    time.plusMinutes(delay()?.delay?.toLong() ?: 0)

@ExperimentalCoroutinesApi
fun Time.delay(): Delay? =
    delays[lines[lineId]?.lineName ?: throw UnknownLineException(lineId)]

@ExperimentalCoroutinesApi
val times: Map<Pair<LineId, StopId>, Time> by lazy {
    readAndSortData("/data/times.csv", Time::key) {
        Time(
            LineId(it[0].toInt()),
            StopId(it[1].toInt()),
            LocalTime.parse(it[2], DateTimeFormatter.ISO_LOCAL_TIME)
        )
    }
}

class UnknownLineException(msg: String) : RuntimeException(msg) {
    constructor(lineId: LineId) : this("Unknown line with ID $lineId")
}

class UnknownStopException(msg: String) : RuntimeException(msg) {
    constructor(stopId: StopId) : this("Unknown stop with ID $stopId")
}
