package de.grannath.verspaetung

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.reactive.result.view.RedirectView
import springfox.documentation.builders.PathSelectors
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger.web.UiConfiguration
import springfox.documentation.swagger.web.UiConfigurationBuilder
import springfox.documentation.swagger2.annotations.EnableSwagger2WebFlux

@SpringBootApplication
@EnableSwagger2WebFlux
class VerspaetungApplication {
    @Bean
    fun petApi(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
            .select()
            .paths(PathSelectors.regex("/lines.*|/stops.*"))
            .build()
            .pathMapping("/")
            .enableUrlTemplating(true)
    }

    @Bean
    fun uiConfig(): UiConfiguration {
        return UiConfigurationBuilder.builder()
            .build()
    }
}

@Controller
class RootController {
    @GetMapping("/")
    suspend fun forwardToUi() =
        RedirectView("/swagger-ui.html")
}

fun main(args: Array<String>) {
    runApplication<VerspaetungApplication>(*args)
}
