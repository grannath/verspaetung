package de.grannath.verspaetung

import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import java.time.LocalTime
import java.time.format.DateTimeParseException

@ExperimentalCoroutinesApi
@RestController
@RequestMapping("stops")
class StopController {
    @GetMapping
    suspend fun getAllStops() =
        stops.values

    @GetMapping("{id}")
    suspend fun getStop(@PathVariable id: Int) =
        stops.values.singleOrNull { it.stopId == StopId(id) } ?: throw UnknownStopException(StopId(id))

    @GetMapping("{id}/times")
    suspend fun getStopTimes(@PathVariable id: Int) =
        times.values
            .filter { it.stopId == StopId(id) }
            .ifEmpty { throw UnknownStopException(StopId(id)) }

    @GetMapping("{id}/nextVehicle")
    suspend fun getNextVehicle(@PathVariable id: Int, @RequestParam time: String?) =
        findNextVehicle(StopId(id), time?.let { LocalTime.parse(it) } ?: LocalTime.now())
}

@ExperimentalCoroutinesApi
@RestController
@RequestMapping("lines")
class LinesController {
    @GetMapping
    suspend fun getAllLines() =
        lines.values

    @GetMapping("{id}")
    suspend fun getLine(@PathVariable id: Int) =
        lines.values.singleOrNull { it.lineId == LineId(id) } ?: throw UnknownLineException(LineId(id))

    @GetMapping("{id}/position")
    suspend fun getVehiclePosition(@PathVariable id: Int, @RequestParam time: String?) =
        findVehiclePosition(LineId(id), time?.let { LocalTime.parse(it) } ?: LocalTime.now())
}

@ControllerAdvice
@ResponseBody
class ExceptionHandler {
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler
    fun handleUnknownStop(e: UnknownStopException) =
        e.message

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler
    fun handleUnknownLine(e: UnknownLineException) =
        e.message

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler
    fun handleUnknownPosition(e: UnknownPositionException) =
        e.message

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler
    fun handleDateTimeParseError(e: DateTimeParseException) =
        "Could not parse time: ${e.message}"
}
